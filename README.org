* Copyright in the Digital Single Market Regulation
  Here is a collection of links regarding the topic. Feel free to contribute.
** EU Resources
   - [[https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:52016PC0593][Proposal for a Directive - 2016]]
   - [[http://www.europarl.europa.eu/sides/getDoc.do?pubRef=-//EP//NONSGML+COMPARL+JURI-OJ-20180620-1+01+DOC+PDF+V0//EN][Agenda of JURI Committee for 20 of June]]
   - [[http://www.europarl.europa.eu/ep-live/en/committees/video?event=20180620-0900-COMMITTEE-JURI][From here you can watch live the meeting and discussions of MEPs on the topic]]
   - [[http://www.europarl.europa.eu/committees/en/juri/members.html?action=0][JURI members]] (no Cypriots here)
   - [[http://www.europarl.europa.eu/committees/en/JURI/amendments.html?action=3][Amendments]]
** Relevant Discussions Among the web
   - [[https://www.reddit.com/r/europe/comments/8q1mfc/on_the_eu_copyright_reform/][Reddit thread]]
   - [[https://news.ycombinator.com/item?id=17338700][HN thread]]
   - [[https://forum.textpattern.io/viewtopic.php?id=48744][forum.textpattern.io thread]]
** Hashtags on Twitter
   - [[https://twitter.com/hashtag/FixCopyright][FixCopyright]]
   - [[https://twitter.com/hashtag/Article13][Article13]]
   - [[https://twitter.com/hashtag/SaveYourInternet][SaveYourInternet]]
** Key Persons
   - [[http://www.europarl.europa.eu/meps/en/124816/JULIA_REDA_home.html][Julia Reda]]
   - [[http://www.europarl.europa.eu/meps/en/96761/AXEL_VOSS_home.html][Axel Voss]]
